import rootReducer from './reducers';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger';

const loggermiddleware = createLogger();

const middleware = (process.env.NODE_ENV === 'development') ? applyMiddleware(
    thunk,
    loggermiddleware,
) : applyMiddleware(thunk);

const Store = createStore(rootReducer, middleware);

export default Store;