import React, { Component } from 'react';
import {history} from '../core'

class Header extends Component {

    handleClick = () => {
        this.props.step.goBack();
    }
    stepCount = (Url) => {
        let content = '';

        switch (Url) {
            case '/':
                content = 'Form step 1'
                return content;
            case '/step-two':
                return content = 'Form step 2'
            case '/step-three':
                return content = 'Form step 3'
            default:
                return ;
        }
    }

    dynamicStep = () => (
        <ul>
            {
                history.location.pathname !== '/' ?
                <li>
                    <button type="button" onClick={this.handleClick} className="btn btn-default">
                        <span className="glyphicon glyphicon-arrow-left" />
                    </button>
                </li>
            : ''
            }
            <li>
                <span>{this.stepCount(history.location.pathname)}</span>
            </li>
        </ul>
        
    )
    render(){
        return (
            <div className="header">
                    {
                        this.dynamicStep()
                         
                    }
            </div>
        )
    }
}

export default Header