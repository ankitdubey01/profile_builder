import React from 'react';
import { Switch, Route } from 'react-router';
import FirstScreen from './components/screens/first-screen'
import SecondScreen from './components/screens/second-screen'
import ThirdScreen  from './components/screens/third_screen';
import NoMatch from './components/404-page'
import { SecurePageRedirect } from './core'

class Routes extends React.Component {
    
    render() {
        // SecurePageRedirect();
        return (
            <Switch>
                <Route exact path="/" component={FirstScreen} />
                <Route path="/step-two" component={SecondScreen} />
                <Route path="/step-three" component={ThirdScreen} />
                <Route component={NoMatch}/>
            </Switch>
        )
    }
}

export default Routes
