import * as constants from '../constants';
import * as services from '../services';

export const checkUsername = (args) =>{
    return async dispatch =>{
        dispatch({
            type: constants.LOAD_USERNAME_REQUEST
        })
        await services.checkUserAvailability(args)
            .then(response=>{
                console.log('response', response);
                if(response.error === false){
                    dispatch({
                        type: constants.LOAD_USERNAME_SUCCESS,
                        payload: response
                    })
                }else{
                    dispatch({
                        type: constants.LOAD_USERNAME_FAILURE
                    })
                    // toastr.error(response.message);
                }
            })
            .catch(error =>{
                dispatch({
                    type: constants.LOAD_USERNAME_FAILURE,
                })
                // alert(error.response.data.message || 'cannot check username');
            })
    }
}

export const clearUsername = (args) =>{
    return dispatch =>{
        dispatch({
            type: constants.CLEAR_REDUCER_DATA
        })
    }
}