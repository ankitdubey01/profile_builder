import * as constants from '../constants';
import * as services from '../services';
import {history} from '../core';
import Store from '../store';

export const addUser = (args) =>{
    return dispatch =>{
        dispatch({
            type: constants.ADD_USER_REQUEST
        })
        services.addUser(args)
                .then(response=>{
                    console.log('response in action', response);
                    if(response.error === false){
                        dispatch({
                            type: constants.ADD_USER_SUCCESS,
                            payload: response
                        })
                        // Store.reset();
                        history.push('/');
                        // toastr.success(response.message);
                    }else{
                        dispatch({
                            type: constants.ADD_USER_FAILURE
                        })
                        history.push('/');
                        // toastr.error(response.data.message);
                    }
                })
                .catch(error =>{
                    dispatch({
                        type: constants.ADD_USER_FAILURE ,
                    })
                    // toastr.error(error.response.data.message || 'Can not add artist', error);
                })
    }
}
