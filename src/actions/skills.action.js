import * as constants from '../constants';
import * as services from '../services';

export const skills = () =>{
    return dispatch =>{
        dispatch({
            type: constants.LOAD_SKILLS_REQUEST
        })
        services.loadSkills()
            .then(response=>{
                console.log('response', response);
                if(response.error === false){
                    dispatch({
                        type: constants.LOAD_SKILLS_SUCCESS,
                        payload: response
                    })
                }else{
                    dispatch({
                        type: constants.LOAD_SKILLS_FAILURE
                    })
                    // toastr.error(response.message);
                }
            })
            .catch(error =>{
                dispatch({
                    type: constants.LOAD_SKILLS_FAILURE,
                })
                alert(error.response.data.message || 'cannot load skills');
            })
    }
}