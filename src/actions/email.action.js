import * as constants from '../constants';
import * as services from '../services';

export const checkEmail = (args) =>{
    return dispatch =>{
        dispatch({
            type: constants.LOAD_EMAIL_REQUEST
        })
        services.checkUserAvailability(args)
            .then(response=>{
                console.log('response', response);
                if(response.error === false){
                    dispatch({
                        type: constants.LOAD_EMAIL_SUCCESS,
                        payload: response
                    })
                }else{
                    dispatch({
                        type: constants.LOAD_EMAIL_FAILURE
                    })
                    // toastr.error(response.message);
                }
            })
            .catch(error =>{
                dispatch({
                    type: constants.LOAD_EMAIL_FAILURE,
                })
                alert(error.response.data.message || 'cannot check email');
            })
    }
}

export const clearUsername = (args) =>{
    return dispatch =>{
        dispatch({
            type: constants.CLEAR_REDUCER_DATA
        })
    }
}