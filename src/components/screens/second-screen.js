import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Form, Field, FieldArray, reduxForm } from 'redux-form';
import Header from '../../containers/header';
import {SecurePageRedirect} from '../../core';
import { Multiselect, renderHobbies, SkipButton  } from '../../core/inputComponents';
import * as actions from '../../actions';

class SecondScreen extends Component {
    
    componentWillMount(){
        SecurePageRedirect();
    }
    StepTwoSubmit = (values) => {
        values['step_two_skip'] = false;
        console.log('step two submit', values);
        this.props.history.push('/step-three');
    }
    componentDidMount() {
        this.props.dispatch(actions.skills());
    }
    
    render(){
        const {history, handleSubmit, submitting, pristine, state} = this.props;
        // console.log('skills', this.props.skills);
        const {skills} = this.props;
        return (
            <Fragment>
                <Header 
                    step={history}
                />
                <div className="container">
                    <div className="row">
                    <div className=" col-md-offset-3 col-md-6" >
                    <h2>General Information</h2>
                    <Form onSubmit={handleSubmit(this.StepTwoSubmit)} >
                        <FieldArray 
                            component={renderHobbies}
                            name="hobbies"
                            label="Your hobbies"
                            placeholder="Hobbies"
                            type="text"
                        />

                        <Field 
                            component={Multiselect}
                            label="Select skills"
                            name="skills"
                            data={skills.skills ? skills.skills.data : []}
                            preFillSkills={state.form.step_form && state.form.step_form.values && state.form.step_form.values.skills}
                        />

                        <Field 
                            component={SkipButton}
                            name="step_two_skip"
                            label="Skip"
                            type="button"
                        />

                        <button 
                        type="submit"
                        disabled={pristine || submitting } 
                        className="btn btn-primary pull-right">
                        next
                        </button>
                    </Form>
                </div>
                </div>
                </div>
            </Fragment>
        )
    }
}

SecondScreen = reduxForm({
    form: 'step_form',
    destroyOnUnmount: false,
    enableReinitialize: true
})(SecondScreen)

function mapStateToProps(state) {
    return {
        state,
        skills : state.skills
    }
}

export default connect(mapStateToProps)(SecondScreen)