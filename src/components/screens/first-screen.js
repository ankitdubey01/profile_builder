import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, Form, reduxForm } from 'redux-form';
import { TextInput } from '../../core/inputComponents';
import { StepOneFormValidate } from '../../core';
import Header from '../../containers/header';
import * as actions from '../../actions';

class FirstScreen extends Component {

    handleNextSubmit = (values) => {
        // console.log('values', values);
        this.props.history.push('/step-two');
    }
    handleUsername = (evt) => {
        console.log('username', evt.target.value);
        const username = evt.target.value;
        if(username.length >= 4){
            let data ={
                username: evt.target.value
            }
            this.props.dispatch(actions.checkUsername(data));
        }
        if(username.length <=4){
            this.props.dispatch(actions.clearUsername())
        }
        
    }

    handleEmail = (evt) => {
        console.log('email', evt.target.value);
        if (/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(evt.target.value)) {
            let data ={
                email: evt.target.value
            }
            this.props.dispatch(actions.checkEmail(data));
        }
        if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(evt.target.value)){
            this.props.dispatch(actions.clearUsername())
        }
        
    }
    // componentDidMount(){
        
    // }

    render(){
        const { handleSubmit, history, pristine, submitting, invalid, emailData, usernameData, userData } = this.props;
        console.log('user data', userData);
        // console.log('email data', this.props.state);
        // if(this.props.userData.user_data){
        //     this.props.reset();
        // }
        return (
            <div>
                <Header />
            <div className="container">
                <div className="row">
                    <div className=" col-md-offset-3 col-md-6" >
                    
                        {userData.user_data && userData.user_data.data ?
                            <div className={`alert ${ userData.user_data.data && userData.user_data.data.length !== 0? 'alert-success':'alert-danger' }`} role="alert">
                                { userData.user_data.message}
                            </div>
                            : ''
                        }
                    <h2>Account information</h2>
                    <Form onSubmit={handleSubmit(this.handleNextSubmit)} >
                        <Field 
                            component={TextInput}
                            label="Name"
                            placeholder="Enter name"
                            name="name"
                            type="text"
                        />
                        <Field 
                            component={TextInput}
                            label="Username"
                            name="username"
                            placeholder="Username"
                            type="text"
                            onChange={this.handleUsername}
                            // checkUsername={(evt) => this.handleUsername(evt)}
                        />
                        <div>
                            {
                                usernameData.username_status && usernameData.username_status.message ? 
                                    <span style={{color: usernameData.username_status.data ? 'red' : 'green'}} >{usernameData.username_status.message}</span> :''
                            }
                            
                        </div>
                        <Field 
                            component={TextInput}
                            label="Email"
                            placeholder="Email"
                            name="email"
                            type="email"
                            onChange={this.handleEmail}
                        />
                        {
                            emailData.email_status && emailData.email_status.message ? 
                                <span style={{color: emailData.email_status.data ? 'red' : 'green'}} >{emailData.email_status.message}</span> :''
                        }

                        <Field 
                            component={TextInput}
                            label="Password"
                            placeholder="password"
                            name="password"
                            type="password"
                        />

                        <Field 
                            component={TextInput}
                            label="Confirm Password"
                            placeholder="Confirm password"
                            name="confirm_password"
                            type="password"
                        />

                        <button 
                        type="submit"
                        disabled={pristine || submitting } 
                        className="btn btn-primary pull-right">next</button>
                    </Form>
                </div>
            </div>
                
            </div>
        </div>
        )
    }
}

FirstScreen = reduxForm({
    form : 'step_form',
    destroyOnUnmount: false,
    validate : StepOneFormValidate
})(FirstScreen)

function mapStateToProps(state) {
    return {
        // state
        usernameData: state.username_data,
        emailData: state.email_data,
        userData: state.userData
    }
}

export default connect(mapStateToProps)(FirstScreen)