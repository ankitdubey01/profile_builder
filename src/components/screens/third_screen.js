import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Form, Field, reduxForm } from 'redux-form';
import Header from '../../containers/header';
import {SecurePageRedirect, FinalStepValidation} from '../../core';
import { ImageUpload, TextAreaInput, SkipButton  } from '../../core/inputComponents';
import * as actions from '../../actions';

class FinalScreen extends Component {
    
    componentWillMount() {
        SecurePageRedirect();
    }

    FinalFormSubmit = (values) => {
        values['skip_and_save'] = false;
        values.hobbies ? values.hobbies.push({hobby: values.hobby}) 
        : values['hobbies'] = values.hobby || []
        console.log('final form submit', values);
        this.props.dispatch(actions.addUser(values));
        this.props.reset();
    }
    
    render(){
        const {history, handleSubmit, submitting, pristine, userData, reset} = this.props;
        console.log('user data', userData); 
        // userData.user_data ? 
        //     this.props.reset()
        //     this.props.history.push('/')
        //     : ''
        return (
            <Fragment>
                <Header 
                    step={history}
                />
                {userData.initiate_submition?
                    <div className="site-loader">
                        <div className="loader">Loading...</div>
                    </div>: ''
                }

                <div className="container">
                    <div className="row">
                    <div className=" col-md-offset-3 col-md-6" >
                    <h2>General Information</h2>
                    <Form onSubmit={handleSubmit(this.FinalFormSubmit)} >
                        <Field 
                            component={ImageUpload}
                            label="Upload profile pic"
                            name="profile_pic"
                        />

                        <Field 
                            component={TextAreaInput}
                            label="Biography"
                            placeholder="Short biography"
                            name="biography"
                            rows={4}
                        />

                        <Field 
                            component={SkipButton}
                            name="skip_and_save"
                            label="Skip & Save"
                            type="button"
                        />
                        {/* <button 
                        type="submit"
                        name="skip_and_save"
                        className="btn btn-default pull-left">
                            Skip & Save
                        </button> */}

                        <button 
                        type="submit"
                        disabled={pristine || submitting } 
                        className="btn btn-primary pull-right">
                            Submit
                        </button>
                </Form>
                </div>
                </div>
                </div>
            </Fragment>
        )
    }
}

FinalScreen = reduxForm({
    form: 'step_form',
    destroyOnUnmount: false,
    validate : FinalStepValidation
})(FinalScreen)

function mapStateToProps(state) {
    return {
        state,
        userData : state.userData
    }
}

export default connect(mapStateToProps)(FinalScreen)