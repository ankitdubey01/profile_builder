import * as constants from '../constants';

let initialValues = {
    load_username : false
}
export const checkUsername = (state = initialValues, action) => {
    switch (action.type) {
        case constants.LOAD_USERNAME_REQUEST:
            state.load_username  = true
            return {
                ...state
            }
        case constants.LOAD_USERNAME_SUCCESS:
            state.load_username  = false
            return {
                ...state,
                username_status: action.payload
            }
        case constants.LOAD_USERNAME_FAILURE:
            state.load_username  = false
            return {
                ...state,
                username_status: ""
            }
        case constants.CLEAR_REDUCER_DATA:
            state.load_username  = false
            return {
                ...state,
                username_status: []
            }
    
        default:
            return {
                ...state
            }
    }
}