import {combineReducers} from 'redux'
import {checkUsername} from './username.reducer';
import {checkEmail} from './email.reducer';
import {skills} from './skills.reducer';
import {addUser} from  './userData.reducer'
import { reducer as reduxFormReducer } from 'redux-form';

export default combineReducers({
    form: reduxFormReducer,
    username_data: checkUsername,
    email_data :  checkEmail,
    skills : skills,
    userData : addUser 
})