import * as constants from '../constants';

let initialValues = {
    load_email : false
}
export const checkEmail = (state = initialValues, action) => {
    switch (action.type) {
        case constants.LOAD_EMAIL_REQUEST:
            state.load_email  = true
            return {
                ...state
            }
        case constants.LOAD_EMAIL_SUCCESS:
            state.load_email  = false
            return {
                ...state,
                email_status: action.payload
            }
        case constants.LOAD_EMAIL_FAILURE:
            state.load_email  = false
            return {
                ...state,
                email_status: ""
            }
        case constants.CLEAR_REDUCER_DATA:
            state.load_email  = false
            return {
                ...state,
                email_status: []
            }
    
        default:
            return {
                ...state
            }
    }
}