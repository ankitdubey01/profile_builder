import * as constants from '../constants';

let initialValues = {
    load_skills : false
}
export const skills = (state = initialValues, action) => {
    switch (action.type) {
        case constants.LOAD_SKILLS_REQUEST:
            state.load_skills  = true
            return {
                ...state
            }
        case constants.LOAD_SKILLS_SUCCESS:
            state.load_skills  = false
            return {
                ...state,
                skills: action.payload
            }
        case constants.LOAD_SKILLS_FAILURE:
            state.load_skills  = false
            return {
                ...state,
                skills: action.payload
            }
    
        default:
            return {
                ...state
            }
    }
}