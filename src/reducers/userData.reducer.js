import * as constants from '../constants';

let initialValues = {
    initiate_submition : false
}
export const addUser = (state = initialValues, action) => {
    switch (action.type) {
        case constants.ADD_USER_REQUEST:
            state.initiate_submition  = true
            return {
                ...state
            }
        case constants.ADD_USER_SUCCESS:
            state.initiate_submition  = false
            return {
                ...state,
                user_data: action.payload
            }
        case constants.ADD_USER_FAILURE:
            state.initiate_submition  = false
            return {
                ...state,
                user_data: []
            }
        case constants.CLEAR_REDUCER_DATA:
            state.initiate_submition  = false
            return {
                ...state,
                user_data: []
            }
    
        default:
            return {
                ...state
            }
    }
}