import {env} from '../env';

export const loadSkills = () =>{
    const Url = `${env.apiUrl}/api/v1/get-skills`
    const headers = {
        'Content-Type': 'application/json'
    }
    return fetch(Url, {
        method: "GET",
        headers: headers
    })
    .then(response => response.json()).then(response =>{
        return response
    })
}