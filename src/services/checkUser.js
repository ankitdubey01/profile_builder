import {env} from '../env';

export const checkUserAvailability = (args) =>{
    console.log('args in service', args);
    let Url  = '';
    if(args.username){
        Url = `${env.apiUrl}/api/v1/user-availability?username=${args.username}`
    }
    if(args.email){
        Url = `${env.apiUrl}/api/v1/user-availability?email=${args.email}`
    }
    
    const headers = {
        'Content-Type': 'application/json'
    }
    return fetch(Url, {
        method: "GET",
        headers: headers
    })
    .then(response => response.json()).then(response => {
        return response
    })
}