import {env} from '../env';

export const addUser = (args) =>{
    console.log('user data in service', args);
    const Url = `${env.apiUrl}/api/v1/addUser`
    // const headers = {
    //     'Content-Type': 'application/json'
    // }
    
    // const skillObject  = Object.assign({}, args.skills);
    // const hobbies  = Object.assign({}, args.hobbies);

    const formData = new FormData();
    for (var i = 0; i < args.hobbies.length; i++) {
        formData.append('hobby[]', args.hobbies[i].hobby);
    }
    if(args.skills){
        for (var i = 0; i < args.skills.length; i++) {
            formData.append('skill[]', args.skills[i].id);
        }
    }
    

    formData.append('profile_pic', args.profile_pic);
    formData.append('name', args.name);
    formData.append('email', args.email);
    formData.append('username', args.username);
    formData.append('password', args.password);
    formData.append('step_two_skip', args.step_two_skip);
    // formData.append('skills', skillObject);
    // formData.append('hobbies', hobbies);
    formData.append('biography', args.biography || "");
    formData.append('skip_and_save', args.skip_and_save);


    return fetch(Url, {
        method: "POST",
        body: formData
    })
    .then(response => response.json()).then(response =>{
        return response
    })
}