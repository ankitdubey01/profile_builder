import React from 'react';
import ReactDOM from 'react-dom';
import {Router} from "react-router";
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import {history} from './core'
import Store from './store'

ReactDOM.render(
    <Provider store={Store}>
        <Router history={history}>
            <App />
        </Router>
        
    </Provider>
    

, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
