import createBrowserHistory from 'history/createBrowserHistory'
import { createMemoryHistory } from 'history';

const browseHistory = createBrowserHistory()

export const history = typeof window === 'undefined'? createMemoryHistory() : browseHistory;