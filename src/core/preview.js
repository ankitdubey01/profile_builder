import React, {Component} from 'react';
import UserImage from '../static/images/user.png';
class ShowPreview extends Component {
    handleRemove = () => {
        let imageInput  = this.props.file;
        imageInput = "";
        this.props.onRemoveFile(imageInput);
    }
    render() {
        const fileUrl = this.props.file ?  
            URL.createObjectURL(this.props.file) 
        : '';

        return (
            <div>
                <div className="profile-pic">
                      <img src={fileUrl !== '' ? fileUrl : UserImage} style={{width: '150px', height: '150px', borderRadius: '50%'}} alt="img" />
                      
                      {fileUrl !== '' ?
                        <button type="button" onClick={this.handleRemove}>remove image</button> 
                      : ''}
                </div>
            </div>
        )
    }
}
export default ShowPreview