import React from 'react';
import Store from '../../store';
import { history } from '../../core';
import {addUser} from '../../actions';

// const handleKeyUp = (checkUsername) => {
//     checkUsername();
// }
const store = Store.getState();

export const TextInput = (field) => (
    <div className="form-group">
        <label>{field.label}</label>
        <div>
            <input 
                className="form-control"
                placeholder={field.placeholder}
                type={field.type}
                onKeyUp={(evt) => field.checkUsername? field.checkUsername(evt) : ''} 
                {...field.input} 
            />
        </div>
        <div>
            {field.meta.touched && field.meta.error &&
            <span style={{color: 'red'}} >{field.meta.error}</span>}
        </div>
    </div>
  )

  export const TextInputForHobby = (field) => (
    <div className="form-group">
        <label>{field.label}</label>
        <div className="input-group" >
            <input 
                className="form-control"
                placeholder={field.placeholder}
                type={field.type}
                autoComplete="none" 
                {...field.input} 
            />
            {
                field.addMore ? 
                <span className="input-group-btn">
                    <button onClick={() => field.onAddField(field.index)} className="btn btn-success" type="button">
                        <span className="glyphicon glyphicon-plus" />
                    </button>
                </span> : 
                <span className="input-group-btn">
                    <button onClick={() => field.onRemoveField(field.index)} className="btn btn-danger" type="button">
                        <span className="glyphicon glyphicon-remove" />
                    </button>
                </span>
            }
            
        </div>
        <div>
            {field.meta.touched && field.meta.error &&
            <span style={{color: 'red'}} >{field.meta.error}</span>}
        </div>
    </div>
  )

  const handleSkip = (field) => {
    field.input.onChange(true);
    // console.log('field ', field);
    if(field.input.name === "skip_and_save"){
        const state = Store.getState();
        const values = state.form.step_form.values;
        values.hobbies ? values.hobbies.push({hobby: values.hobby}) 
        : values['hobbies'] = values.hobby || []
        console.log('form values in skip and save', values);
        // console.log('errors', state.form.step_form);
        if(!state.form.step_form.syncErrors){
            Store.dispatch(addUser(values));
            history.push('/');
        }
        
        return false;
    }
    history.push('/step-three');

  }

  export const SkipButton = (field) => (
        <div>
            <button 
                className="btn btn-warning"
                type={field.type}
                onClick={() => handleSkip(field)} 
                {...field.input} 
            >{field.label}</button>
        </div>
  )

//   export const SkipAndSave = (field) => (
//     <div>
//         <button 
//             className="btn btn-warning"
//             type={field.type}
//             onClick={() => handleStepTwoSkip(field)} 
//             {...field.input} 
//         >{field.label}</button>
//     </div>
// )