import React from 'react';

export const TextAreaInput = (field) => (
    <div className="form-group">
        <label>{field.label}</label>
        <div>
            <textarea 
                className="form-control"
                placeholder={field.placeholder}
                rows={field.rows}
                {...field.input} 
            ></textarea>
        </div>
        <div>
            {field.meta.touched && field.meta.error &&
            <span style={{color: 'red'}} >{field.meta.error}</span>}
        </div>
    </div>
  )