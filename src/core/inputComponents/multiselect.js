import React from 'react';
import Select, { components } from 'react-select';


// const options = [
//     { value: 'chocolate', label: 'Chocolate' },
//     { value: 'strawberry', label: 'Strawberry' },
//     { value: 'vanilla', label: 'Vanilla' }
//   ];

  const DropdownIndicator = (props) => (
      <components.DropdownIndicator {...props}>
      </components.DropdownIndicator>
    );

export const Multiselect = ({input , label, data, preFillSkills, meta}) => {
    return <div className="form-group">
        <label>{label}</label>
        <div>
        <Select
            closeMenuOnSelect={false}
            components={{ DropdownIndicator }}
            defaultValue={preFillSkills}
            isMulti
            onChange={(values) => input.onChange(values)}
            options={data}
        />
        </div>
        <div>
            {meta.touched && meta.error &&
            <span style={{color: 'red'}} >{meta.error}</span>}
        </div>
    </div>
};

