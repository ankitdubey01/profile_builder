import React from 'react';
import ShowPreview from '../preview';

export class ImageUpload extends React.Component {
    
    adaptFileEventToValue = (e) =>{
        return this.props.input.onChange(e.target.files[0])
    }

    render(){
        const {label, meta, input} = this.props;
        console.log('input', input);
        return (
            <div className="form-group">
                <label>{label}</label>
            <div>
            <input
                className="form-control"
                onChange={this.adaptFileEventToValue}
                type="file"
                id="image_file"
            />
            </div>
            <div>
                {meta.touched && meta.error &&
                <span style={{color: 'red'}} >{meta.error}</span>}
            </div>
            <div>
             {
                 typeof(meta.error) === 'undefined' ?
                    <ShowPreview 
                        file={input.value}
                        onRemoveFile={(evt) =>{ input.onChange(evt)
                            document.getElementById('image_file').value = ""
                        }}
                    /> : ''
             }   
            </div>
            </div>
        )
    }
}