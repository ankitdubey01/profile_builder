import React from 'react';
import  {Field}  from 'redux-form';
import {TextInput, TextInputForHobby} from './textInput'

const required = value => (value || typeof value === 'number' ? undefined : 'Required')

export class renderHobbies extends React.Component {
  Add_more_hobbies = (e)=> {
    if (e.altKey && e.keyCode == 65) {
        // console.log('control key pressed ')
        this.props.fields.push({});
    }
  }
  componentDidMount(){
    document.addEventListener('keyup', this.Add_more_hobbies, false);
  }
  render(){
    const { fields, label, type, placeholder, meta: { touched, error } } = this.props;
    
    return (
          <div>
            <div>
              <Field
                label={label} 
                component={TextInputForHobby}
                name="hobby"
                placeholder={placeholder}
                addMore={true}
                onAddField={() => fields.push({})}
              />
            {touched && error && <span>{error}</span>}
            </div>

          {fields.map((member, index) =>
          <div key={index}>
              <Field
                  label={index+2 + ' hobby'}
                  name={`${member}.hobby`}
                  type={type}
                  component={TextInputForHobby}
                  placeholder={placeholder}
                  validate={[required]}
                  index={index}
                  onRemoveField={(index) => fields.remove(index)}
              />
          </div>
          )}
          </div>
    )
  }
} 
  