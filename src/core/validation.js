export const StepOneFormValidate = values => {

    let name_regex = /^[a-zA-Z ]*$/;
    const errors = {}

    //Name validation
    if(!values.name){
        errors.name = 'Required field'
    }else if(values.name.length < 5){
        errors.name = 'Name must be greater than four characters';
    }else if (name_regex.test(values.name) === false) {
      errors.name = 'Name should not contain special characters or number';
    }

    //Email validation
    if (!values.email) {
        errors.email = 'Required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address'
    }

    //Username validation
    if (!values.username) {
        errors.username = 'Required'
    }else if(values.username.length < 5){
        errors.username = 'Username must be greater than four characters';
    }else if (!/^[-\w\.\$@\*\!]{1,30}$/i.test(values.username)) {
        errors.username = 'Invalid username!. username should not have space'
    }else if(/^[0-9]/.test(values.username)){
        errors.username = 'Username can\'t be only numeric'
    }

    //Password validation
    if(!values.password) {
        errors.password = 'Password is required'
    }else if (values.password.length < 6){
        errors.password = 'Password should atleast 6 characters'
    }

    //Confirm validation
    if(!values.confirm_password) {
        errors.confirm_password = 'Confirm password is required'
    }else if (values.confirm_password !== values.password){
        errors.confirm_password = 'Password mismatched'
    }


    return errors
  }

  export const FinalStepValidation = values => {
    const fileType = ['image/jpg', 'image/jpeg', 'image/png'];
    const errors = {}

    //Profile pic size and file extension validation
    if(values.profile_pic && values.profile_pic.size  > 1000000){
        errors.profile_pic = "file size exceed limit"
    }else if(values.profile_pic && fileType.indexOf(values.profile_pic.type) === -1){
        errors.profile_pic = "Unsupported file type! Only jpg, jpeg and png allowed"
    }

    return errors
  }